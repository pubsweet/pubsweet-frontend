# DEPRECATED: this project has been renamed to pubsweet-client

The new repo can be found at https://gitlab.coko.foundation/pubsweet/pubsweet-client.

This repository has been deprecated, but will be preserved here for compatibility of older installs.